#ejercicio 2

def mayor_numero(num1, num2):
    if (num1 > num2):
        resultado = num1
    elif (num2 > num1):
        resultado = num2

    return resultado
        
#********
#main
#********
        
if __name__ == "__main__":
    mayor_numero(8,4)
    lista = [7,4,5,6]
    print(mayor_numero(lista[0],lista[1]))
    print(mayor_numero(lista[2],lista[3]))
    print(mayor_numero(mayor_numero(lista[0],lista[1]),mayor_numero(lista[2],lista[3])))
